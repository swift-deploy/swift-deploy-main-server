package com.swiftdeploy.swiftdeploy.Repositories;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.swiftdeploy.swiftdeploy.Models.LogModel;

@Repository
public interface LogRepository extends JpaRepository<LogModel, UUID> {

    List<LogModel> findByDeploymentId(String deploymentId);

    @Query("SELECT l FROM LogModel l WHERE l.deploymentId = :deploymentId ORDER BY l.createdAt DESC")
    List<LogModel> findByDeploymentIdOrderByCreatedAtDesc(@Param("deploymentId") String deploymentId);
}
