package com.swiftdeploy.swiftdeploy.KafkaLogs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

import com.swiftdeploy.swiftdeploy.Models.LogModel;
import com.swiftdeploy.swiftdeploy.Repositories.LogRepository;

@Service
public class LogsConsumer {

    @Autowired
    private LogRepository logRepository;
    
    @SuppressWarnings("unused")
    @Autowired
    private Environment env;

    @KafkaListener(topics = "${TOPIC}", groupId = "${GROUP_ID}")
    public void getLogs(@Header(KafkaHeaders.RECEIVED_KEY) String key, String value) {

        LogModel logModel = new LogModel();
        logModel.setDeploymentId(key);
        logModel.setLog(value);
        logRepository.save(logModel);
    }

}
