package com.swiftdeploy.swiftdeploy.Middlewares;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.swiftdeploy.swiftdeploy.Models.ResponseMessage;

@Service
public class CreateRecord {

    @Autowired
    private WebClient.Builder webClientBuilder;

    @Autowired
    private ResponseMessage responseMessage;

    public Object createRecordMethod(String slug) {
        try {
            String recordURL = System.getenv("RECORD_URL");

            @SuppressWarnings("unchecked")
            Map<String, String> response = webClientBuilder.build()
                    .post()
                    .uri(recordURL)
                    .header("slug", slug)
                    .retrieve()
                    .bodyToMono(Map.class)
                    .block();

            return response;

        } catch (Exception e) {
            responseMessage.setSuccess(false);
            responseMessage.setMessage("Internal Server Error: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseMessage);
        }
    }

}
