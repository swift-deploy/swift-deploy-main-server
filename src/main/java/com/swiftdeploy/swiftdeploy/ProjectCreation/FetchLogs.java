package com.swiftdeploy.swiftdeploy.ProjectCreation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestHeader;

import com.swiftdeploy.swiftdeploy.Models.LogModel;
import com.swiftdeploy.swiftdeploy.Models.ResponseMessage;
import com.swiftdeploy.swiftdeploy.Repositories.LogRepository;

@Service
public class FetchLogs {

    @Autowired
    private LogRepository logRepository;

    public ResponseEntity<Object> fetchLogs(@RequestHeader String deploymentId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            List<LogModel> logs = logRepository.findByDeploymentIdOrderByCreatedAtDesc(deploymentId);
            if (logs.isEmpty()) {
                responseMessage.setSuccess(false);
                responseMessage.setMessage("Logs haven't generated yet! Please wait for a while.");
                return ResponseEntity.status(200).body(responseMessage);
            } else {
                return ResponseEntity.status(200).body(logs);
            }
        } catch (Exception e) {
            responseMessage.setSuccess(false);
            responseMessage.setMessage("Internal Server Error. Reason: " + e.getMessage());
            return ResponseEntity.status(500).body(responseMessage);
        }
    }

}
