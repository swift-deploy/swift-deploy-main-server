package com.swiftdeploy.swiftdeploy.ProjectCreation;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.swiftdeploy.swiftdeploy.Authentication.AuthService;
import com.swiftdeploy.swiftdeploy.Middlewares.CreateRecord;
import com.swiftdeploy.swiftdeploy.Middlewares.SlugGenerator;
import com.swiftdeploy.swiftdeploy.Models.DeploymentModel;
import com.swiftdeploy.swiftdeploy.Models.LogModel;
import com.swiftdeploy.swiftdeploy.Models.ProjectModel;
import com.swiftdeploy.swiftdeploy.Models.ResponseMessage;
import com.swiftdeploy.swiftdeploy.Repositories.DeploymentRepository;
import com.swiftdeploy.swiftdeploy.Repositories.LogRepository;
import com.swiftdeploy.swiftdeploy.Repositories.ProjectRepository;
import com.swiftdeploy.swiftdeploy.Repositories.UserRepository;
import com.swiftdeploy.swiftdeploy.StaticInformations.ECSClientService;

import software.amazon.awssdk.services.ecs.model.AssignPublicIp;
import software.amazon.awssdk.services.ecs.model.AwsVpcConfiguration;
import software.amazon.awssdk.services.ecs.model.ContainerOverride;
import software.amazon.awssdk.services.ecs.model.KeyValuePair;
import software.amazon.awssdk.services.ecs.model.LaunchType;
import software.amazon.awssdk.services.ecs.model.NetworkConfiguration;
import software.amazon.awssdk.services.ecs.model.RunTaskRequest;
import software.amazon.awssdk.services.ecs.model.TaskOverride;

@Service
public class DeployProject {

        @Autowired
        private DeploymentRepository deploymentRepository;

        @Autowired
        private ProjectRepository projectRepository;

        @Autowired
        private UserRepository userRepository;

        @Autowired
        private AuthService authService;

        @Autowired
        private SlugGenerator slugGenerator;

        @Autowired
        private LogRepository logRepository;

        @Autowired
        private CreateRecord createRecord;

        public ResponseEntity<ResponseMessage> createDeployService(String projectId, String token,
                        Map<String, String> envVariablesByClient) {

                ResponseMessage responseMessage = new ResponseMessage();
                try {

                        DeploymentModel deployment = new DeploymentModel();

                        ProjectModel project = projectRepository.findById(UUID.fromString(projectId)).orElse(null);
                        if (project == null) {
                                responseMessage.setMessage("Project not found");
                                return ResponseEntity.status(404).body(responseMessage);
                        }

                        DeploymentModel deploymentExists = deploymentRepository.findByProject(project);
                        UUID id;

                        if (deploymentExists != null) {

                                id = deploymentExists.getDeploymentId();

                                List<LogModel> oldLogs = logRepository.findByDeploymentId(id.toString());
                                logRepository.deleteAll(oldLogs);
                                
                                responseMessage.setSuccess(true);
                                responseMessage.setMessage(
                                                "Deployment already exists. Overwriting with the latest changes");

                        } else {
                                UUID randomId = UUID.randomUUID();
                                id = randomId;
                                deployment.setDeploymentId(id);
                                deployment.setProject(project);
                                deployment.setCreatedAt(LocalDate.now());

                                String slug = slugGenerator.reverseProxyMethod().getBody().getMessage();

                                createRecord.createRecordMethod(slug);

                                project.setDomain(slug);

                                projectRepository.save(project);
                        }

                        List<KeyValuePair> envVariables = new ArrayList<>();
                        envVariables.add(
                                        KeyValuePair.builder().name("BUCKET_NAME").value(System.getenv("BUCKET_NAME"))
                                                        .build());

                        envVariables.add(KeyValuePair.builder().name("AWS_ACCESS_KEY_ID")
                                        .value(System.getenv("AWS_ACCESS_KEY"))
                                        .build());

                        envVariables.add(KeyValuePair.builder().name("AWS_SECRET_ACCESS_KEY")
                                        .value(System.getenv("AWS_SECRET_KEY")).build());

                        envVariables.add(KeyValuePair.builder().name("GIT_URL")
                                        .value(projectRepository.findByProjectId(UUID.fromString(projectId))
                                                        .getGitURL())
                                        .build());

                        envVariables.add(KeyValuePair.builder().name("USER_ID")
                                        .value(userRepository.findByEmail(authService.verifyToken(token)).getUserId()
                                                        .toString())
                                        .build());

                        envVariables.add(KeyValuePair.builder().name("PROJECT_ID")
                                        .value(projectId).build());

                        envVariables.add(KeyValuePair.builder().name("DEPLOYMENT_ID")
                                        .value(id.toString()).build());

                        envVariables.add(KeyValuePair.builder().name("TOPIC")
                                        .value(System.getenv("TOPIC")).build());

                        envVariables.add(KeyValuePair.builder().name("LISTERNER")
                                        .value(System.getenv("LISTERNER")).build());

                        envVariables.add(KeyValuePair.builder().name("GROUP_ID")
                                        .value(System.getenv("GROUP_ID")).build());

                        envVariables.add(KeyValuePair.builder().name("BOOTSTRAP_SERVER")
                                        .value(System.getenv("BOOTSTRAP_SERVER")).build());

                        for (Map.Entry<String, String> entry : envVariablesByClient.entrySet()) {
                                envVariables.add(KeyValuePair.builder().name(entry.getKey()).value(entry.getValue())
                                                .build());
                        }

                        String existingTaskDefinitionArn = "arn:aws:ecs:" + System.getenv("REGION") + ":"
                                        + System.getenv("ACCOUNT_ID") + ":task-definition/"
                                        + System.getenv("TASK_DEFINITION_NAME");

                        ContainerOverride containerOverride = ContainerOverride.builder()
                                        .name(System.getenv("IMAGE_NAME"))
                                        .environment(envVariables)
                                        .build();

                        TaskOverride taskOverride = TaskOverride.builder()
                                        .containerOverrides(containerOverride)
                                        .build();

                        String[] subnets = System.getenv("SUBNETS").split(",");

                        RunTaskRequest runTaskRequest = RunTaskRequest.builder()
                                        .cluster(System.getenv("CLUSTER_NAME"))
                                        .taskDefinition(existingTaskDefinitionArn)
                                        .overrides(taskOverride)
                                        .networkConfiguration(NetworkConfiguration.builder()
                                                        .awsvpcConfiguration(AwsVpcConfiguration.builder()
                                                                        .subnets(subnets)
                                                                        .assignPublicIp(AssignPublicIp.ENABLED)
                                                                        .build())
                                                        .build())
                                        .launchType(LaunchType.FARGATE)
                                        .build();

                        ECSClientService.ecsClient.runTask(runTaskRequest);

                        if (deploymentExists != null) {
                                return ResponseEntity.status(200).body(responseMessage);
                        } else {
                                deploymentRepository.save(deployment);
                                responseMessage.setSuccess(true);
                                responseMessage.setMessage("Deployment created successfully");
                                return ResponseEntity.status(200).body(responseMessage);
                        }

                } catch (Exception e) {
                        responseMessage.setSuccess(false);
                        responseMessage.setMessage("Internal Server Error. Reason: " + e.getMessage());
                        return ResponseEntity.status(500).body(responseMessage);
                }
        }
}
