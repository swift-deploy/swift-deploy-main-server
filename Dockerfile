FROM ubuntu:latest

RUN apt update

RUN apt install openjdk-17-jdk -y

RUN apt install maven -y

WORKDIR /swift-deploy

COPY . /swift-deploy/

ENTRYPOINT [ "mvn","spring-boot:run" ]